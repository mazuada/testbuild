const express =require('express');
const app= express();
const port= process.env.PORT || 3000;

const bodyParser =require('body-parser');
app.use(bodyParser.json()); //utiliza el parse de json
app.listen(port);
console.log("API escuchando en el puerto" + port);

const userController = require('./controllers/userControllers');
const authController = require('./controllers/Authcontrollers');

app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"hola desde API Techu AUTOMATICO!!!!!!"});
  }
);


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body); //necesita un pareseador

  }
);

app.get('/apitechu/v1/usuarios', userController.getUsersV1);
app.post ('/apitechu/v1/usuario', userController.createUserV1);
app.delete("/apitechu/v1/usuario/:id", userController.deleteUserV1);
app.get('/apitechu/v2/usuarios', userController.getUserV2);

app.get("/apitechu/v2/usuario/:id", userController.getUserbyIdV2);
app.post ('/apitechu/v2/usuario', userController.createUserV2);
app.post('/apitechu/v1/login',authController.loginV1);
app.post('/apitechu/v1/logout',authController.logoutV1);
app.post('/apitechu/v2/login',authController.loginV2);
