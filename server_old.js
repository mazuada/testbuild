const express =require('express');
const app= express();
const port= process.env.PORT || 3000;
const bodyParser =require('body-parser');
app.use(bodyParser.json());//utiliza el parse de json
app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"hola desde API TechU"});
  }
);

/*app.get('/apitechu/v1/users',
  function(req,res){
    var users= require('./users.json');
    var
    console.log("GET /apitechu/v1/users");
    //res.send(users);
    var cont =users.length;
    for (user of users){
      console.log (req.query.top);
      console.log(cont);
      if(cont != req.query.top) {
        users.splice(user, users.Length); //borro el último elemento del array
      }
      else {
        res.send(users);
        break;
      }
      cont --;
    }
  }
); */

app.get("/apitechu/v1/users",
 function(req, res) {
   console.log("GET /apitechu/v1/users");
   console.log(req.query);

   var result = {};
   var users = require('./users.json');
   console.log("prueba de que va bien");
   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length;
   }

   result.users = req.query.$top ?
      users.slice(0, req.query.$top) : users;
      res.send(result);

 }
);
/*app.get('/apitechu/v1/5users',
  function(req,res){
    var users= require('./users.json');
    console.log("GET /apitechu/v1/5users");
    res.send(users);*/

    /*for (user of users) {
    console.log("Length of array is "+  users.length);
    if (user != null && req.params.id == user.id) {
      console.log("La id coincide");
      delete users[user.id -1];
      break;
    }
  }
}
);*/

app.post("/apitechu/v1/users",
  function(req,res){
    console.log("POST /apitechu/v1/users");
    console.log("first_name:"+ req.body.first_name);
    console.log("last_name:" + req.body.last_name);
    console.log("email" + req.body.email);

    var newUser = {
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email
    };
    var users = require('./users.json');
    users.push(newUser); //lo guarda en binario
    writeUserDataToFile(users);

    console.log("usuario añadido con éxito");
    res.send({"msg":"Usuario añadido con exito"});

  }
);
  //  app.delete("/apitechu/v1/users/:id",
  //  function(req, res) {
  //    console.log("DELETE apitechu/v1/users:id");
  //    console.log("id es "+  req.params.id);

//      var users =require('./users.json');
//      users.splice(req.params.id -1, 1);
//      writeUserDataToFile(users);
//      console.log("Usuario borrado");
//      res.send({"msg":"Usuario borrado"});


  //  }
//);

app.delete("/apitechu/v1/users:id",
function(req, res) {
  console.log("DELETE apitechu/v1/users:id");
  console.log("id es "+  req.params.id);
  var users =require('./users.json');
  /*for (user of users) {
    console.log("Length of array is "+  users.length);
    if (user != null && req.params.id == user.id) {
      console.log("La id coincide");
      delete users[user.id -1];
      break;
    }
  } */

  for (arrayId in users){

    if (users[arrayId].id == req.params.id){
      console.log("se ha borrado el usuario de id " + users[arrayId].id);
      users.splice(arrayId,1);
      break;
   }
  }
  writeUserDataToFile(users);
  console.log("Usuario borrado");
  res.send({"msg":"Usuario borrado"});
  }
  );

function writeUserDataToFile(data) {
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./users.json",jsonUserData, "utf-8",
    function(err) {
      if (err){
        console.log(err);
      } else {
        console.log("Datos escritos en fichero");
      }
    }
)
}
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body); //necesita un pareseador

  }
);
