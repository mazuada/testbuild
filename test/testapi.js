const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../server'); //así se inicia la api

describe('First unit test',
 function() {
   it('Test that Duckduckgo works', function(done) {
       chai.request('http://www.duckduckgo.com')
         .get('/')
         .end(
           function(err, res) {
             console.log("Request has finished");
             res.should.have.status(200);
             done();
           }
         )
     }
   )
 }
)

describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de Usuarios responde', function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1/users')
         .end(
           function(err, res) {
             console.log("Request has finished");
             res.should.have.status(200);
             done();
           }
         )
     }
   ),
   it('Prueba que la API de Usuarios lista de usuarios correcta', function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1/users')
         .end(
           function(err, res) {
             console.log("Request has finished");
             res.should.have.status(200);
             res.body.users.should.be.a("array"); //lo que esta dentro del body debe ser de tipo array

             for( user of res.body.users){
              user.should.have.property('email');
              user.should.have.property('password');
              }
             done();
           }
         )
     }
   )
 }
)
