const io =require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechumazuada6ed/collections/";
const mLabAPIKey = "apiKey=8FjAtkP7hrqRkNYorpSJsknu8L4KIh5V";

function loginV1(req,res){
           console.log("POST /apitechu/v1/login" );
           var usuarios = require('../usuarios.json');
           var result={};
           for (user of usuarios) {
                    if (user.email == req.body.email) {
                    console.log("encontrado " + req.body.email);
                     if (user.password == req.body.password) {
                          console.log("password ok ");
                          user.logged = true;
                          result.mensaje= "Login correcto";
                          result.id=user.id;
                          io.writeUserDataToFile(usuarios);
                          console.log("usuario logado añadido");

                             break;
                       } else {
                         console.log("password KO ");
                         result.mensaje="login incocrrecto";
                         //res.send({"msg" : "Login Incorrecto"});
                         break;
                         }
                 }
             }
             res.send(result);
         }

         function logoutV1(req,res){
                console.log("POST /apitechu/v1/logout" );
                var usuarios = require('../usuarios.json');
                var result={};

                for (user of usuarios) {
                         if (user.id == req.body.id && user.logged == true){
                              console.log("encontrado " + req.body.id);
                              delete user.logged;
                              io.writeUserDataToFile(usuarios);
                              result.mensaje= "logout correcto";
                              result.id=user.id;
                              break;
                            } else {
                              mensaje= "Logout Incorrecto";
                              }
                      }
                        res.send(result);
                  }

/******************                  ************************************/
function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email": "' + email + '"}';
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log('leyendo usuario de la BBDD');

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
            "msg" : "Error."
        }
        res.status(500);
      } else {
         if (body.length > 0)
          {
           console.log (body[0]);
          //chequeamos el password
          var isPasswordcorrecta =  crypt.checkPassword(password,body[0].password);
          console.log (isPasswordcorrecta);
          if (isPasswordcorrecta)  {
          //actualizar el usuario con la id concreta
          var query2 = 'q={"id" : ' + body[0].id +'}';
          var putBody = '{"$set":{"logged":true}}';

                      var httpClient =requestJson.createClient(baseMlabURL);
                       httpClient.put("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
                       function(errPUT, resMLabPUT, bodyPUT) {
                      console.log("Usuario logado con exito");
                      var response = bodyPUT [0];
                         }
                       )
                     }
               }
         else {
          var response = {
            "msg" : " usuario no encontrado"
          }
          res.status(404);
        }
      }
            res.send(response);
    }
  );


}

/******************************************************************/

         module.exports.loginV1=loginV1;
         module.exports.logoutV1=logoutV1;
         module.exports.loginV2=loginV2;
