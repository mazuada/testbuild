const io =require('../io');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechumazuada6ed/collections/";
const mLabAPIKey = "apiKey=8FjAtkP7hrqRkNYorpSJsknu8L4KIh5V";
const crypt = require('../crypt');

function getUsersV1(req, res) {
   console.log("GET /apitechu/v1/usuarios");
   var result = {};
   var users = require('../usuarios.json');

   result.cont = users.length;
   result.users = users;
   console.log('numero de usuarios' + result.cont);
   res.send(result.users);
 }

function createUserV1(req,res){
  console.log('POST /apitechu/v1/usuario');
  var nuevoUsuario = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };
  var usuarios = require ('../usuarios.json');
  usuarios.push(nuevoUsuario);
  io.writeUserDataToFile(usuarios);
  res.send('{"msg":"usuario añadido con éxito"}');
}


function deleteUserV1(req, res) {
  console.log("DELETE apitechu/v1/users:id");
  console.log("id es "+  req.params.id);
  var users =require('../usuarios.json');
    for (arrayId in users){
    if (users[arrayId].id == req.params.id){
      console.log("se ha borrado el usuario de id " + users[arrayId].id);
      users.splice(arrayId,1);
      break;
   }
  }
  io.writeUserDataToFile(users);
  console.log("Usuario borrado");
  res.send({"msg":"Usuario borrado"});
}

function getUserV2(req, res) {
   console.log("GET /apitechu/v2/usuarios");

   var httpClient = requestJson.createClient(baseMlabURL);
   console.log('leyendo usuarios de la BBDD');
   httpClient.get("user?" +mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
            }
            res.send(response);
    }
 );
 }

 function getUserbyIdV2(req, res) {
    console.log("GET /apitechu/v2/usuario/:id");
    var id = req.params.id;
    var query = 'q={"id": ' + id + '}';
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('leyendo usuario de la BBDD');
    httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         var response = {
             "msg" : "Error obteniendo usuarios."
         }
         res.status(500);
       } else {
          if (body.length > 0) {
                  response = body [0];
                }
          else {
           var response = {
             "msg" : " usuario no encontrado"
           }
           res.status(404);
         }
       }
             res.send(response);
     } );
}

function createUserV2(req,res){
  console.log('POST /apitechu/v2/usuario');

  var nuevoUsuario = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  };

var httpClient =requestJson.createClient(baseMlabURL);
console.log('Cliente creado');
console.log('id' + req.body.id);
console.log('email' + req.body.email);

  httpClient.post("user?" + mLabAPIKey, nuevoUsuario,
   function(err, resMLab, body) {


     console.log("Usuario creado con exito");
       res.send ({"msg" : "Usuario creado con exito"})
     }
   )
 }


  module.exports.getUsersV1= getUsersV1;
  module.exports.createUserV1= createUserV1;
  module.exports.deleteUserV1 = deleteUserV1;
  module.exports.getUserV2 = getUserV2;
  module.exports.getUserbyIdV2= getUserbyIdV2;
  module.exports.createUserV2= createUserV2;
